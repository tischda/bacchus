---
grade: |
    Département Informatique

    5\ieme{} année

    2013 - 2014

document:
    type: Analyse statistique multivariée
    title: R-Bacchus - ACP avec le logiciel R
    short: Analyse en Composantes Principales
    abstract: |
        Chemical analysis of wines grown in the same region in Italy but derived
        from three different cultivars. The analysis determined the quantities
        of 13 constituents found in each of the three types of wines.

    keywords: PCA, classification, R, multivariate, wine
    frenchabstract: |

        Analyse en composantes principales de données chimiques de vins italiens.

    frenchkeywords: ACP, classification, R, analyse de données, multivariée


supervisors:
    category: Encadrant
    list:
        - name: Gilles VENTURINI
          mail: gilles.venturini@univ-tours.fr
    details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
    list:
        - name: Daniel TISCHER
          mail: daniel.tischer@etu.univ-tours.fr
    details: DI5 2013 - 2014

preamble:
    include.tex
---

Introduction
============

L'analyse de données
--------------------

L'analyse de données recouvre des techniques permettant la description
statistique de grands tableaux. L'objectif est de résumer ces données en
mettant en évidence un nombre restreint de nouvelles variables synthétiques,
obtenues par la combinaison linéaire des variables initiales.

Dans le cas des problèmes _descriptifs_, il s'agit de représenter les objets
dans un espace de dimension compatible avec la perception humaine (3 axes au
maximum). Les méthodes associées à cette classe de problèmes sont:

* L'analyse en composantes principales (ACP)
* L'analyse factorielle des correspondances (AFC)
* L'analyse factorielle discriminante (AFD)

Pour les problèmes _décisionnels_, on souhaite en général affecter un objet à
une classe et leur résolution fait souvent appel à une analyse descriptive en
vue de construire les classes en présence.

L'analyse en composantes principales
------------------------------------

L'ACP est une méthode d'analyse multivariée exploratoire : on cherche à
structurer et à résumer l'information pour mieux l'appréhender. La projection
des données sur des espaces de dimension réduite permet de visualiser
l'organisation prépondérante des données. Une composante principale peut être
considérée comme le représentant (la synthèse) d'un groupe de variables liées
entre elles.

$$X=\begin{bmatrix} x_{1,1} & \cdots & x_{1,k} \\ \vdots & \ddots & \vdots \\ x_{n,1} & \cdots & x_{n,k}\end{bmatrix}$$

Si nous considérons le tableau $X$ comme le nuage de points correspondant aux
observations de $k$ variables sur $n$ individus, l'ACP trouve les directions de
dispersion maximale du nuage des individus et du nuage de variables. Cela permet
des représentations dans des espaces de plus faible dimension tout en conservant
un maximum de dispersion (ou d'_inertie_) du nuage.

L'individu i étant représenté par le vecteur $e_i$ à $k$ composantes
$e_i = (x_{i,1} \cdots x_{i,k})^\top$, l'inertie totale du nuage de points est
la moyenne des carrés des distances des points au centre de gravité :

$$I_g = \sum_{i=1}^n p_i \| e_i - g \|^2$$

Notons que $p_i$ est le poids associé à chaque individu (en général ce sera la
valeur $\frac{1}{n}$).

Transformation des données
--------------------------

Dans l'ACP normée (la plus couramment utilisée), les valeurs du tableau des
données sont centrées et réduites. Ainsi, on soustrait à chaque valeur numérique
la moyenne de la variable, et on divise le résultat par son écart type $S_k$. Le
tableau obtenu a pour terme général :

$$ \frac{(x_{i,k} - \bar{x}_k)}{S_k}$$

Ce tableau peut être considéré de deux façons différentes :

1. Un individu est une suite de $k$ nombres et peut être représenté dans un
point de l'espace vectoriel à $k$ dimensions, noté $R^k$, dont chaque dimension
représente une variable. On s'intéresse aux distances inter-individuelles qui
s'interprètent comme des ressemblances. Du fait du centrage, l'origine des axes
est confondue avec le centre de gravité du nuage.

![](images/nuage-individus.png)

2. Une variable est une suite de $n$ nombres et peut être représentée par un
vecteur de $R^n$. Dans ce nuage, on s'intéresse surtout aux angles entre les
variables. Le cosinus d'un angle entre deux variables s'interprète comme le
coefficient de corrélation entre les deux variables. La réduction permet de
s'affranchir de l'arbitraire des unités de mesure. De ce fait, toutes les
variables sont équidistantes de l'origine et donc situées sur une hypersphère
de rayon 1.

![](images/nuage-variables.png)

Analyse factorielle
-------------------

L'analyse factorielle d'un nuage consiste à mettre en évidence une suite de
directions telles que l'inertie, par rapport à l'origine, de la projection du
nuage sur ces directions soit maximum. Dans $R^k$, les axes factoriels sont les
directions d'allongement maximum. Dans $R^n$, ce sont les variables synthétiques
les plus liées à l'ensemble des variables initiales.

Choix du nombre d'axes
----------------------

Le choix du nombre d'axes factoriels à retenir lors d'une ACP revient à décider
à partir de quel ordre $\alpha$ les différences entre les pourcentages d'inertie
ne sont plus significatives. Les projections sur les axes supérieurs à $\alpha$
ne seront alors plus utilisées dans l'interprétation. Sachant que l'inertie d'un
axe est donnée par la valeur propre correspondante, une solution est d'examiner
la courbe de décroissance des valeurs propres pour déterminer les points où la
courbe forme un coude.

![](images/choix-axes.png)

Seuls les axes qui précèdent ce changement de pente seront retenus.

Qualité de l'approximation
--------------------------

Les axes factoriels fournissant des images approchées, il est nécessaire de
mesurer la qualité de l'approximation. Pour cela, on utilise le pourcentage
d'inertie associé à un axe qui donne aussi l'importance relative d'un axe dans
la variabilité des données :

$$Q = \frac{\text{inertie de la projection}}{\text{inertie totale du nuage}}$$

Un autre rapport permet de mettre en évidence les éléments qui ont contribué
principalement à la construction de l'axe. Avec $\psi_{is}$ pour la coordonnée
du point-ligne $i$ sur l'axe $s$, et $\lambda_s$ pour l'inertie de la projection
de l'ensemble du nuage sur l'axe $s$, on peut mesurer la part d'inertie d'un
point-ligne par la formule :

$$CTR(i,s)=\frac{p_i \psi_{is}^2}{\lambda_s}}$$

Données et problématique
========================

Les données
-----------

Les données sont les résultats d'une analyse chimique de trois variétés de vins
produits dans une même région d'Italie. L'analyse de 178 instances a déterminé
les quantités de 13 composants trouvés dans chacun des trois types de vins :

    Alcohol
    Malic acid
    Ash
    Alcalinity of ash
    Magnesium
    Total phenols
    Flavanoids
    Nonflavanoid phenols
    Proanthocyanins
    Color intensity
    Hue
    OD280/OD315 of diluted wines
    Proline

Les données peuvent être obtenues sur le _Machine Learning Repository_ à
l'adresse suivante :

[http://archive.ics.uci.edu/ml/datasets/Wine](http://archive.ics.uci.edu/ml/datasets/Wine)

Objectif
-------

L'objectif est de déterminer des critères de classification des vins avec
l'analyse en composantes principales.

Le logiciel R
-------------

R est un logiciel de développement scientifique spécialisé dans le calcul et
l’analyse statistique. C'est un projet open-source  sous licence GNU GPL
[[http://www.r-project.org/](http://www.r-project.org/)] qui s'inspire du
langage S développé en 1976 par Bell Laboratories.

R est aussi un environnement facilement extensible avec de nombreux _packages_
disponibles au travers des sites du CRAN (Comprehensive R Archive Network).

### Installation de R

L'installation de R 3.1.0 ne pose pas de problème particulier, ni sous Windows
ni sous MacOS. Pour notre analyse, nous avons besoin de quelques packages
additionnels.

#### ggbiplot

Le package ggbiplot [[http://www.vince.vu/software/](http://www.vince.vu/software/)]
n'est disponible qu'au format source. Il faut donc d'abord installer
[Rtools](http://cran.r-project.org/bin/windows/Rtools). On exécute ensuite la
séquence d'actions suivante :

~~~
find_rtools()
install.packages("devtools")
library(devtools)
install_github("ggbiplot", "vqv")
~~~

#### FactoMineR

FactoMineR [[http://factominer.free.fr/](http://factominer.free.fr/)] est un
package R dédié à l'analyse exploratoire multidimensionnelle de données
développé et maintenu par F. Husson, J. Josse, S. Lê, d'Agrocampus Rennes, et J.
Mazet.

Pour l'installation du package et de son interface graphique, on exécutera la
séquence suivante :

~~~
install.packages("FactoMineR")
source("http://factominer.free.fr/install-facto.r")
~~~

### Installation de R-Studio

R-Studio [[http://www.rstudio.com/](http://www.rstudio.com/)] est un
environnement de développement intégré (IDE) qui facilite grandement le
développement sous R. Notons qu'il existe aussi sous forme d'application web.

\begin{figure}[!h]
  \centering
  \includegraphics[width=12cm]{images/rstudio.png}
\end{figure}


Analyse des données
===================

Traitement
----------

Après le lancement de R-Studio, on entre les commandes directement dans la
fenêtre de console. La première chose à faire est de lire les données avec la
fonction `read.table` en spécifiant la virgule comme séparateur :

~~~
wine<-read.table("http://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data",sep=",")
~~~

On peut maintenant consulter ces données, par exemple en affichant les 5
premières lignes :

~~~
> wine[1:5,]
  V1    V2   V3   V4   V5  V6   V7   V8   V9  V10  V11  V12  V13  V14
1  1 14.23 1.71 2.43 15.6 127 2.80 3.06 0.28 2.29 5.64 1.04 3.92 1065
2  1 13.20 1.78 2.14 11.2 100 2.65 2.76 0.26 1.28 4.38 1.05 3.40 1050
3  1 13.16 2.36 2.67 18.6 101 2.80 3.24 0.30 2.81 5.68 1.03 3.17 1185
4  1 14.37 1.95 2.50 16.8 113 3.85 3.49 0.24 2.18 7.80 0.86 3.45 1480
5  1 13.24 2.59 2.87 21.0 118 2.80 2.69 0.39 1.82 4.32 1.04 2.93  735
~~~

Nous avons au total 178 lignes et 14 colonnes, la première colonne servant à
identifier le type de vin et les 13 suivantes les concentrations des composants
chimiques.

~~~
> dim(wine)
[1] 178  14
~~~

Avec R il est facile d'appliquer un calcul statistique sur chacune des
variables. Pour cela, on utilise la fonction `sapply()`. Par exemple, pour
calculer la moyenne, on applique la fonction `mean` sur les colonnes 2 à 14 :

~~~
> sapply(wine[2:14], mean)
         V2          V3          V4          V5          V6          V7          V8
 13.0006180   2.3363483   2.3665169  19.4949438  99.7415730   2.2951124   2.0292697
         V9         V10         V11         V12         V13         V14
  0.3618539   1.5908989   5.0580899   0.9574494   2.6116854 746.8932584
~~~

De même pour l'écart type (sd \to _standard deviation_) :

~~~
> sapply(wine[2:14], sd)
         V2          V3          V4          V5          V6          V7          V8
  0.8118265   1.1171461   0.2743440   3.3395638  14.2824835   0.6258510   0.9988587
         V9         V10         V11         V12         V13         V14
  0.1244533   0.5723589   2.3182859   0.2285716   0.7099904 314.9074743
~~~

Les écarts types sont très différents ($V14 = 314.9$ et $V9 = 0.12$), mais pour
l'ACP on ne veut pas tenir compte de l'ordre de grandeur des variables, mais
seulement de la façon dont elles se dispersent les unes par rapport aux autres.
Nous allons donc standardiser les variables, c'est à dire les centrer et les
réduire, de façon à ce que leur variance soit égale à 1 et leur moyenne égale à
0. Pour cela, on utilise la fonction `scale()` :

~~~
concentrations <- as.data.frame(scale(wine[2:14]))
~~~

Vérifions le résultat pour la moyenne, qui devrait être proche de zéro :

~~~
> sapply(concentrations, mean)
           V2            V3            V4            V5            V6            V7
-8.591766e-16 -6.776446e-17  8.045176e-16 -7.720494e-17 -4.073935e-17 -1.395560e-17
           V8            V9           V10           V11           V12           V13
 6.958263e-17 -1.042186e-16 -1.221369e-16  3.649376e-17  2.093741e-16  3.003459e-16
          V14
-1.034429e-16
~~~

Et pour l'écart type :

~~~
> sapply(concentrations, sd)
 V2  V3  V4  V5  V6  V7  V8  V9 V10 V11 V12 V13 V14
  1   1   1   1   1   1   1   1   1   1   1   1   1
~~~

Voici comment exécuter l'ACP (ou directement avec standardisation : `prcomp(wine[2:14], scale=TRUE)`).

~~~
> wine.pca <- prcomp(concentrations)
> wine.pca
Standard deviations:
 [1] 2.1692972 1.5801816 1.2025273 0.9586313 0.9237035 0.8010350 0.7423128 0.5903367
 [9] 0.5374755 0.5009017 0.4751722 0.4108165 0.3215244

Rotation:
             PC1          PC2         PC3         PC4         PC5         PC6         PC7
V2  -0.144329395  0.483651548 -0.20738262  0.01785630 -0.26566365  0.21353865 -0.05639636
V3   0.245187580  0.224930935  0.08901289 -0.53689028  0.03521363  0.53681385  0.42052391
V4   0.002051061  0.316068814  0.62622390  0.21417556 -0.14302547  0.15447466 -0.14917061
V5   0.239320405 -0.010590502  0.61208035 -0.06085941  0.06610294 -0.10082451 -0.28696914
...
~~~

La matrice de rotation représente les vecteurs propres. On obtient le même
résultat en cherchant les valeurs propres de la matrice des covariances avec :

~~~
> wine.eigen <- eigen(cov(concentrations))
> wine.eigen
$values
 [1] 4.7058503 2.4969737 1.4460720 0.9189739 0.8532282 0.6416570 0.5510283 0.3484974
 [9] 0.2888799 0.2509025 0.2257886 0.1687702 0.1033779

$vectors
              [,1]         [,2]        [,3]        [,4]        [,5]        [,6]
 [1,] -0.144329395 -0.483651548 -0.20738262 -0.01785630  0.26566365  0.21353865
 [2,]  0.245187580 -0.224930935  0.08901289  0.53689028 -0.03521363  0.53681385
 [3,]  0.002051061 -0.316068814  0.62622390 -0.21417556  0.14302547  0.15447466
 [4,]  0.239320405  0.010590502  0.61208035  0.06085941 -0.06610294 -0.10082451
 ...
~~~

Pour décider quelles composantes doivent être retenues, on cherche un coude
dans le diagramme suivant :

~~~
screeplot(wine.pca, type="lines")
~~~

\begin{figure}[!h]
  \centering
  \includegraphics[width=8cm]{images/eboulis-crop.pdf}
\end{figure}

On voit qu'un changement de pente s'effectue à partir de la composante 4, on
peut donc ne retenir que les trois premières composantes (le pourcentage de
variance est égal au pourcentage d'inertie).

Les valeurs des composantes principales sont stockées dans les colonnes de la
matrice $x$ de l'objet retourné par `prcomp()`. Pour représenter les deux
premières composantes sur deux axes différents, on exécute :

~~~
plot(wine.pca$x[,1],wine.pca$x[,2], pch=20, cex=0.8)
text(wine.pca$x[,1],wine.pca$x[,2], wine$V1, cex=0.6, pos=4, col="red")
~~~

\begin{figure}[!h]
  \centering
  \includegraphics[width=9cm]{images/scatter-crop.pdf}
\end{figure}

On voit ici que les échantillons de vins de type 1 ont des valeurs plus basses
dans la première composante principale (axe des abscisses) que ceux du type 3.
Ainsi la première composante sépare les échantillons de type 1 et 3.

On voit également que les vins de type 2 ont des valeurs plus basses que les
types 1 et 3 sur l'axe des ordonnées. Par conséquent, la deuxième composante
sépare les échantillons de type 2 des échantillons de types 1 et 3.

Deux composantes seraient donc suffisantes pour séparer les trois types de vin.


Visualisation avancée
---------------------

### Traitement des données

FactoMineR propose des méthodes d'analyses factorielles et de classification
avec une interface et un module graphique. Pour commencer, nous allons charger
la librairie `FactoMineR` et labéliser les colonnes.

~~~
library(FactoMineR)

colnames(wine) = c("Cultivar", "Alcohol", "MalicAcid", "Ash", "AlcAsh", "Mg", "Phenols",
                   "Flav", "NonFlaPhenols", "Proa", "Color", "Hue", "OD", "Proline")
~~~

Les _factors_ sont des variables R qui prennent un nombre limité de valeurs. En
tant que variables de catégorisation, elles sont traitées différemment des
autres valeurs par les fonctions statistiques. Avec la commande suivante, on
affecte le type _factor_ à la variable de catégorisation _Cultivar_ (= variété
végétale agricole).

~~~
wine$Cultivar[wine$Cultivar == 1] <- "Barolo"
wine$Cultivar[wine$Cultivar == 2] <- "Grignolino"
wine$Cultivar[wine$Cultivar == 3] <- "Barbera"
wine[1] <- lapply(wine[1], as.factor)
~~~

On exécute maintenant l'analyse en gardant 3 composantes principales (`ncp=3`),
et en demandant la standardisation des variables à l'aide du paramètre optionnel
`scale.unit=TRUE` :

~~~
res.pca <- PCA(wine, scale.unit=TRUE, quali.sup=1, ncp=3, graph=FALSE)
~~~

Avec l'option `quali.sup=1` on précise que la première colonne est une variable
qualitative supplémentaire qui ne participera donc pas à la construction des
axes.

Comme auparavant, on détermine le nombre d'axes à retenir en fonction du
pourcentage de la variance (qui correspond à l'inertie que l'on veut maximiser).

~~~
> res.pca$eig
        eigenvalue percentage of variance cumulative percentage of variance
comp 1   4.7058503             36.1988481                          36.19885
comp 2   2.4969737             19.2074903                          55.40634
comp 3   1.4460720             11.1236305                          66.52997
comp 4   0.9189739              7.0690302                          73.59900
comp 5   0.8532282              6.5632937                          80.16229
comp 6   0.6416570              4.9358233                          85.09812
comp 7   0.5510283              4.2386793                          89.33680
comp 8   0.3484974              2.6807489                          92.01754
comp 9   0.2888799              2.2221534                          94.23970
comp 10  0.2509025              1.9300191                          96.16972
comp 11  0.2257886              1.7368357                          97.90655
comp 12  0.1687702              1.2982326                          99.20479
comp 13  0.1033779              0.7952149                         100.00000
~~~

Un changement significatif se distingue entre la troisième et la quatrième
composante, on décide donc de ne retenir que les trois premières.

### Graphe des individus

Affichons le graphe des individus en prenant la colonne 1 pour l'habillage (la
coloration) des données :

~~~
plot(res.pca, cex=0.5, habillage=1, invisible="quali", select="cos2 0.5")
~~~

\begin{figure}[!ht]
  \centering
  \includegraphics[width=9.5cm]{images/individus-crop.pdf}
\end{figure}

L'option `select="cos2 0.5"` limite l'affichage des labels aux individus dont la
qualité de représentation (cosinus carré) dans le plan est > 0.5. Si cette
dernière est proche de 1, le point représentant l'individu est très proche de
l'axe ou du plan, sa distance au centre de gravité est alors bien visible sur
la projection.

### Graphe des variables

~~~
plot(res.pca, choix="var", shadow=TRUE)
~~~

\begin{figure}[!ht]
  \centering
  \includegraphics[width=9.5cm]{images/variables-crop.pdf}
\end{figure}

Cette représentation permet de décrire les axes à partir des variables du jeu de
données, par exemple `Dim 1` en fonction de `Flav`, `Phenols` et `OD`. Avec un
coefficient de corrélation de 0.9174702, une valeur proche de 1, la variable
`Flav` est très liée à la première dimension et permet donc de caractériser cet
axe.

\lstset{basicstyle={\ttfamily\scriptsize}}

~~~
dimdesc(res.pca, axes=1:2)

$Dim.1                                               $Dim.2
              correlation      p.value                         correlation      p.value
Flav            0.9174702 0.000000e+00               Color       0.8374894 0.000000e+00
Phenols         0.8561367 0.000000e+00               Alcohol     0.7642573 0.000000e+00
OD              0.8160189 0.000000e+00               Proline     0.5766127 0.000000e+00
Proa            0.6799217 0.000000e+00               Ash         0.4994461 1.278089e-12
Hue             0.6436621 0.000000e+00               Mg          0.4734761 2.478462e-11
Proline         0.6220508 0.000000e+00               MalicAcid   0.3554317 1.124118e-06
Alcohol         0.3130934 2.088257e-05               OD         -0.2599338 4.584234e-04
Mg              0.3080229 2.880155e-05               Hue        -0.4412422 7.060723e-10
Color          -0.1922360 1.014930e-02
AlcAsh         -0.5191571 1.132941e-13
NonFlaPhenols  -0.6476070 1.532702e-22
~~~

La combinaison des deux graphiques s'obtient avec la librairie `ggbiplot`
selon les calculs effectués par `prcomp()` dans l'analyse de données décrite
dans la partie 3 (graphique inversé par rapport à FactoMineR) :

~~~
library(ggbiplot)
g <- ggbiplot(wine.pca, obs.scale=1, var.scale=1, groups=wine$Cultivar, ellipse=TRUE, circle=TRUE)
g <- g + scale_color_discrete(name='')
g <- g + theme(legend.direction='horizontal', legend.position='top')
print(g)
~~~

\lstset{basicstyle={\ttfamily\footnotesize}}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=12.5cm]{images/ggbiplot-crop.pdf}
\end{figure}


Analyse des résultats
---------------------

### Différences

Ce qui surprend tout d'abord, c'est la représentation inversée des graphiques.
En effet, dans FactoMineR les valeurs des composantes principales 1, 4 et 5 sont
multipliées par `-1` :

~~~
> wine.pca$x[1:5,1:5]
           PC1        PC2        PC3        PC4        PC5
[1,] -3.307421  1.4394023 -0.1652728  0.2150246  0.6910933
[2,] -2.203250 -0.3324551 -2.0207571  0.2905387 -0.2569299
[3,] -2.509661  1.0282507  0.9800541 -0.7228632 -0.2503270
[4,] -3.746497  2.7486184 -0.1756962 -0.5663856 -0.3109644
[5,] -1.006070  0.8673840  2.0209873  0.4086131  0.2976180
>
>
> res.pca$ind$coord[1:5,]
     Dim.1      Dim.2      Dim.3      Dim.4      Dim.5
1 3.316751  1.4434626 -0.1657390 -0.2156312 -0.6930428
2 2.209465 -0.3333929 -2.0264574 -0.2913583  0.2576546
3 2.516740  1.0311513  0.9828187  0.7249023  0.2510331
4 3.757066  2.7563719 -0.1761918  0.5679833  0.3118416
5 1.008908  0.8698308  2.0266882 -0.4097658 -0.2984575
~~~

La réponse apportée sur le
[forum](https://groups.google.com/forum/#!topic/factominer-users/9SQN4oh1SOg) de
FactoMinerR est la suivante :

>    _"Il n'y a pas de bon ou de mauvais, le sens des axes est arbitraire, ce qui
>    vous intéresse c'est la direction de plus grande variabilité. Selon les
>    logiciels, vous pouvez trouver des graphiques inversés mais
>    l'interprétation des graphiques est exactement la même. Vous n'interprétez
>    pas la valeur d'une coordonnée en particulier."_


### Classification

L'ACP peut servir à la sélection de caractéristiques pour réduire la dimension
de l'espace de données. Comme base de comparaison, j'applique la méthode des K
plus proches voisins sur un échantillon de 50 individus sélectionnés
aléatoirement :

~~~
classes <- wine$Cultivar
train <- sample(1:dim(concentrations)[1], 50)
result1 <- knn(concentrations[train,], concentrations[-train,], classes[train], k=1)
table1 <- table(old=classes[-train], new=result1)
> table1
            new
old          Barbera Barolo Grignolino
  Barbera         33      0          0
  Barolo           0     43          0
  Grignolino       4      6         42
> chisq.test(table1)$statistic
X-squared
 204.7462
~~~

Si l'on réduit l'espace de données aux deux dimensions déterminées par l'ACP,
en ne sélectionnant que les deux variables qui caractérisent le mieux chacun
des axes (cf. valeurs retournées par `dimdesc`), on obtient la matrice de
confusion suivante :

~~~
> reduced <- scale(wine[,c("Flav", "Color")])
> result2 <- knn(reduced[train,], reduced[-train,], classes[train], k=1)
> table2 <- table(old=classes[-train], new=result2)
> table2
            new
old          Barbera Barolo Grignolino
  Barbera         32      0          1
  Barolo           0     42          1
  Grignolino       1      5         46
> chisq.test(table2)$statistic
X-squared
 214.1224
~~~

La valeur supérieure de `X-squared` indique des classes mieux dissociées, c'est
à dire une classification plus efficace avec seulement un sixième (2/13) des
caractéristiques du jeu de données initial.


Conclusion
----------

Ce rapport est une introduction au problème d'interprétation de grands jeux de
données par l'application concrète d'outils mathématiques (calcul matriciel,
métriques, projections). En effet, il existe des liens entre les notions de
variance, de covariance, et d'inertie d'un nuage de points qui peuvent se
visualiser de façon plus intuitive avec des outils comme R et des packages
spécialisés.

Nous avons étudié en détail l'analyse en composantes principales (ACP) sur
l'exemple des vins italiens. Par étapes, nous sommes arrivés à visualiser les
relations de dualité qui lient les nuages de points des individus à ceux des
variables. Ceci permet de mieux comprendre la représentation synthétique
présentée en dernier.

Enfin, nous avons appliqué une réduction de dimension de l'espace de données
grâce aux axes factoriels déterminés par l'ACP. Ceci nous a permis d'obtenir de
bons résultats malgré une réduction importante des données. La limite de cette
approche est qu'elle cache des caractéristiques qui contribuent peu à la
variance dans les données, ce qui peut éradiquer des différenciateurs
significatifs qui pourraient, eux, affecter les performances d'un modèle de
classification automatique.


\nocite{escofier_analyses_2008}
\nocite{crucianu_methodes_2004}
\nocite{govaert_analyse_2003}
\nocite{saporta_probabilites_2006}
\nocite{husson_cours_2014}
\nocite{sherman_k-means_????}
\nocite{_wine_????}
\nocite{garcia_multivariate_2011}
\nocite{_test_????}
\nocite{perrin_contribution_2008}
\nocite{_lanalyse_????}
\nocite{mabey_pca_????}
\nocite{forina_multivariate_1986}
\nocite{hoffmann_principal_2011}
\nocite{desgraupes_livre_2013}
