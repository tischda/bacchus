@echo off
pdfcrop ggbiplot.pdf

del /q ggbiplot.log
del /q ggbiplot.pdf
del /a ggbiplot.aux


pdfcrop eboulis.pdf

del /q eboulis.log
del /q eboulis.pdf
del /a eboulis.aux


REM plot(wine.pca$x[,1],wine.pca$x[,2], pch=20, cex=0.6, cex.lab=0.8, cex.axis=0.8)
REM text(wine.pca$x[,1],wine.pca$x[,2], wine$V1, cex=0.5, pos=4, col="red")

pdfcrop scatter.pdf

del /q scatter.log
del /q scatter.pdf
del /a scatter.aux


REM par (cex=0.8)
REM plot(res.pca, cex=0.6, habillage=1, invisible="quali", select="cos2 0.5", title="")

pdfcrop individus.pdf

del /q individus.log
del /q individus.pdf
del /a individus.aux


REM par (cex=0.8)
REM plot(res.pca, choix="var", shadow=TRUE, title="")

pdfcrop variables.pdf

del /q variables.log
del /q variables.pdf
del /a variables.aux
