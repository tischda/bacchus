::-----------------------------------------------------------------------------
:: This script transforms Markdown and LaTeX sources into PDF
::-----------------------------------------------------------------------------
:: Usage:
::   make                  compiles md and tex sources in ./src
::   make clean            removes build files
::-----------------------------------------------------------------------------
@echo off
setlocal
set BUILD_DIR=build

set PANDOC_OPTS=-V classoption=UTF8
set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=twoside
:: set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=fouside
:: set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=landscape
:: set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=draft
:: set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=final
:: set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=CDS

if "%1"=="clean" goto clean

if not exist %BUILD_DIR% md %BUILD_DIR%

::-----------------------------------------------------------------------------
:main

echo . Processing Markdown
set INPUT=rapport-bacchus-acp.md bibliographie.md
call scripts\compile-md.bat %INPUT% > nul 2>&1

REM echo . Processing LaTeX
REM set INPUT=modele-rapport-tex.tex
REM call scripts\compile-tex.bat %INPUT% > nul 2>&1

goto end
::-----------------------------------------------------------------------------
:clean

echo . Cleaning %BUILD_DIR% directory
rd /s/q %BUILD_DIR%

:end
endlocal
