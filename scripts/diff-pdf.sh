#!/bin/bash
# ------------------------------------------------------------------------------
# diff-pdf.sh - compares 2 PDF files and higthlights changes in output file.
#
# Requires: Bash        (Windows: cygwin)
#           ImageMagick (Windows: ImageMagick-6.8.9-0-Q16-x64-dll.exe)
#           pdfinfo     (Windows: comes with msysgit)
#           GhostScript (Windows: rename gswin64c.exe to gs.exe)
#
# http://tex.stackexchange.com/questions/36351/comparing-the-output-of-two-pdfs
# ------------------------------------------------------------------------------
if [ $# -eq 0 ] ; then
    echo Usage: $0 \<file1.pdf file2.pdf [diff.pdf]\>
	exit 1
fi

DENSITY=150

FILEA=$1
FILEB=$2
OUTFILE=$3

if [[ -z "$OUTFILE" ]]; then
    OUTFILE="diff.pdf"
fi

# count pages in each document
PAGESA=$(pdfinfo "$FILEA" | grep ^Pages: | sed 's/Pages:\s*//')
PAGESB=$(pdfinfo "$FILEB" | grep ^Pages: | sed 's/Pages:\s*//')

if [[ $PAGESA > $PAGESB ]];
then
    PAGES=$(($PAGESA-1))
else
    PAGES=$(($PAGESB-1))
fi

# compare documents page by page
for N in `seq -w 0 "$PAGES"`; do
    echo "Comparing page $N / $PAGES."
    compare -quiet -metric PSNR -density "$DENSITY" "$FILEA[$N]" "$FILEB[$N]" "$OUTFILE~page~${N}" 2> /dev/null;
done

# merge page diff output files
gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=$OUTFILE `ls $OUTFILE~page~*`

# clean up
rm -f $OUTFILE~page~*
